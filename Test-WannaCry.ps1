$MVer = 6.1 
$ProgressPreference = 'SilentlyContinue'

$tpvalue = '401221[2-8]|4012598|4012606|4013198|4013429|4015217|4015438|401554[69]|401555[02]|4016635|4019215|401926[34]|4019472'

Set-Service wuauserv -StartupType automatic
Start-service wuauserv 

$Session = New-Object -ComObject 'Microsoft.Update.Session'
$Searcher = $Session.CreateUpdateSearcher()
$FormatEnumerationLimit = -1
$historyCount = $Searcher.GetTotalHistoryCount()

if ($historyCount -gt 0) {
    Write-Verbose 'WU Patch History Available - Selecting History of patches installed, limited to operation "Installation" and resultcode "InProgress, Succeeded and SucceededWithErrors"'
    Write-Verbose 'Limiting final results to only include the Title'
    $wuKBLIST = $($Searcher.QueryHistory(0, $historyCount)|Select-Object Title, Date, Operation, Resultcode|Where-Object {$_.Operation -like 1 -and $_.Resultcode -match '[123]'}| Select-object -ExpandProperty Title)
}

if((Get-Command Get-Hotfix).Count -gt 0){
    $ghfKBLIST = $(Get-Hotfix -ErrorAction SilentlyContinue | Where-object {$_.hotfixid -match 'KB\d{6,7}'} | Select-object -ExpandProperty Hotfixid)
}

If ($wuKBLIST -eq $null -and $ghfKBLIST -eq $null) {
    Write-Output 'WARNING - No updates returned - Get-Hotfix and Windows Update History both returned no usable data'
} 
else {
    Write-Verbose 'Filtering $results from above, containing either only Update Titles, or HotfixIDs'
    $finalKBLIST = $wuKBLIST + $ghfKBLIST  |
    Where-Object {$_ -match 'KB('+$tpvalue+')' -or 
        ( $_ -match '^((2017-0[3-9]|2017-1[0-2]|201[89]-[0-9]{2})|(Ma|A|Ju|[SOND][^ ]+ber).* 2017 |[a-z]{3,10} 201[89] )' -and 
          $_  -match '(Security .*Rollup|Cumulative Update) for Windows')};
    If ($finalKBLIST -eq $null) {
        Write-Output 'Vulnerable - Couldnt make a regex match or a name match'
    } 
    else {
        Write-Output "Secured - Detected Updates: $(($finalKBLIST | Select-String 'KB\d{6,7}' -AllMatches | ForEach-Object {$_.matches} | ForEach-Object {$_.Value} | Select-Object -Unique) -join ',')"
    }
}

